var mongoose = require('mongoose');
var crypto = require('crypto');
var db = mongoose.connect("mongodb://localhost/test",{ useMongoClient: true });
var User = require('./db/models/User.js');

db.on('error', function (err) {
    console.log('connection error:', err.message);
});

db.once('open', function callback () {
    console.log("Connected to DB!");
});


// User API

exports.createUser = function(userData){
    var user = {
        userName: userData.userName,
        password: hash(userData.password)
    };
    return new User(user).save();
};

exports.getUser = function(id) {
    return User.findOne(id);
};

exports.checkUser = function(userData) {
    return User
        .findOne({userName: userData.userName})
        .then(function(doc){
            if ( doc && doc.password === hash(userData.password) ){
                console.log("User password is ok");
                return Promise.resolve(doc);
            } else {
                console.log("User password is wrong");
                return Promise.reject("Error wrong");
            }
        })
};


function hash(text) {
    return crypto.createHash('sha1')
        .update(text).digest('base64');
}
