var newsrc = "csharp.png";

function changeImage() {
    if ( newsrc == "csharp.png" ) {
        document.images["csharp"].src = "/images/csharp.png";
        document.images["csharp"].alt = "CSharp";
        newsrc  = "csharp_alt.png";
    }
    else {
        document.images["csharp"].src = "/images/csharp_alt.jpg";
        document.images["csharp"].alt = "CSharp";
        newsrc  = "csharp.png";
    }
}

function doc(src) {

    var content = '';
    switch (src){
        case "csharp":
        {
            content = '<b>CSharp example</b><br>' +
                'git clone https://github.com/ingress2018/asptest.git<br>' +
                'cd asptest<br>' +
                'git remote add dokku ssh://dokku@dev-gpu.sikorsky.dynabic.com/asptest<br>' +
                '<br>' +
                'git push dokku master' +
                '';
            break;
        }
        case "java":
        {
            content = '<b>Java example</b><br>' +
                'git clone https://github.com/kissaten/webapp-runner-minimal.git<br>' +
                'cd webapp-runner-minimal<br>' +
                'git remote add dokku ssh://dokku@dev-gpu.sikorsky.dynabic.com/webapp-runner-minimal<br>' +
                '<br>' +
                'git push dokku master' +
                '';
            break;
        }
        case "node":
        {
            content = '<b>Node.js example</b><br>' +
                'git clone https://github.com/heroku/node-js-sample.git<br>' +
                'cd node-js-sample<br>' +
                'git remote add dokku ssh://dokku@dev-gpu.sikorsky.dynabic.com/node-js-sample<br>' +
                '<br>' +
                'git push dokku master' +
                '';
            break;
        }
        case "python":
        {
            content = '<b>Python example</b><br>' +
                'git clone https://github.com/ovhlabs/python-dokku-sample.git<br>' +
                'cd python-dokku-sample<br>' +
                'git remote add dokku ssh://dokku@dev-gpu.sikorsky.dynabic.com/python-dokku-sample<br>' +
                '<br>' +
                'git push dokku master' +
                '';
            break;
        }
        case "ruby":
        {
            content = '<b>Ruby example</b><br>' +
                'git clone https://github.com/rchrd2/dokku-ruby-example.git<br>' +
                'cd dokku-ruby-example<br>' +
                'git remote add dokku ssh://dokku@dev-gpu.sikorsky.dynabic.com/dokku-ruby-example<br>' +
                '<br>' +
                'git push dokku master' +
                '';
            break;
        }
    }
    document.getElementById("description").innerHTML = content;
}