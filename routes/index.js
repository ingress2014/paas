var express = require('express');
var router = express.Router();
var api = require('../api');

/* GET home page. */
router.get('/', function(req, res, next) {
   var data;
    if(req.session.user){
     data = {
         title: 'Platform as Service:',
         user : req.session.user
     };
     console.log('User session is - ' + req.session.user);
     res.render('index', data);
     } else {
         data = { title: 'Login page'};
         res.render('login', data);
     }
});

router.post('/', function(req, res, next){
    res.redirect('/');
});

router.get('/login', function(req, res, next) {
    res.render('login',{title:'Login page'});
});

router.get('/logout', function(req, res, next) {
    if (req.session.user) {
        delete req.session.user;
        res.redirect('/');
//        res.redirect('/login');
//        res.render('login');
    }
});


router.get('/register', function(req, res, next) {
    res.render('registration',{title:'Register page'});
});

router.get('/admin', function(req, res, next) {
    res.redirect('http://dev-gpu.sikorsky.dynabic.com:9000');
});

router.get('/docs', function(req, res, next) {
    var data;
    if(req.session.user) {
        data = { title: 'Documentation:', user: req.session.user};
    }
    res.render('docs', data);
});

router.get('/info', function(req, res, next) {
    res.render('info');
});


module.exports = router;