var express = require('express');
var router = express.Router();
var api = require('../api.js');
var alert;

/* Create user */
router.post('/login', function(req, res, next) {
//    if (req.session.user) return res.redirect('/');

    api.checkUser(req.body)
        .then(
            function(user){
            if(user){
                console.log('Registered user. Login is Ok');
                req.session.user = {id: user._id, name: user.name};
                res.redirect('/');
            } else {
                console.log('User not registered.Bad login or pass');
                res.render('registration',{title:'Register page'});
                //return next(error);
            }
        },
            function(){
                console.log("Not Authorized");
                res.render('registration',{title:'Register page'});
            }
        )
        .catch(function(error){
            return next(error);
        })

});

router.post('/', function(req, res, next) {
    console.log('Registered users');
    if (req.body.userName == '') {
        res.render('info',{message : 'Login is empty'});
    } else if (req.body.password == '') {
        res.render('info',{message : 'Password is empty'});
    } else if (req.body.key == '') {
        res.render('info',{message : 'Key is empty'});
    } else {

        //Save user and set public key
        var userName = req.body.userName.trim();
        var password = req.body.password.trim();
        var key = req.body.key.trim();
        console.log('userName - ' + userName);
        console.log('Password - ' + password);
        console.log('Public key - ' + key);

        try {
            //run command for save public key in system
            var cmd = "echo '" +key + "' | dokku ssh-keys:add " + userName;
            console.log(cmd);
            var result = require('child_process').execSync(cmd);
            console.log(result);

            api.createUser(req.body)
                .then(function () {
                    console.log("User created");
                    var data = {
                        title: 'PaaS:',
                        user: req.session.user
                    };
                    res.render('index', data);
                })
                .catch(function (err) {
                    if (err.toJSON().code == 11000) {
                        res.render('info',{message : "This userName already exist"});
                    } else {
                        res.render('info',{message : "Unexpected error"});
                    }
                });
        }catch(err){
            res.render('info', {message : err.name + ' public key is bad or ' +
                'already present in system'});
        }
    }
});

module.exports = router;